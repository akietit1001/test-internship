import './App.css';
import { useState } from 'react';
import { Radio, Tabs} from 'antd';

function App() {
  const [value, setValue] = useState(0);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const questions = [
    'Khi truy cập 1 trang web (VD: https://geekadaventure.vn) trình duyệt sẽ làm gì',
    'Nếu có 2 CSS styles có selector cùng ứng với 1 element trên cây DOM thì style của element sẽ thể hiện như thế nào?',
    'Tại sao JavaScript có cơ chế xử lý bất đồng bộ (Asynchronous)? Có những cách nào để xử lý bất đồng bộ trong JavaScript?'
  ]

  const answers = [
    'Khi truy cập 1 trang web thì trình duyệt sẽ gọi tới máy chủ DNS để biên dịch URL trang web thành địa chỉ IP, mỗi trang web có địa chỉ IP riêng biệt. Trình duyệt sẽ dùng đại chỉ IP đó để gửi request tới Server lưu trữ trang web. Nếu Server chấp nhận thì sẽ gửi lại thông báo 200 OK. Và sau đó trình duyệt sẽ truy xuất mã HTML của trang web cụ thể được yêu cầu. Khi trình duyệt nhận được mã HTML đó từ Server thì nó sẽ hiện thị ra cửa sổ của trình duyệt một trang web hoàn chỉnh.',
    'Khi một phần tử HTML chịu tác động của nhiều bộ chọn với nhiều giá trị giống nhau ví dụ như định kiểu màu sắc cho chữ, thì nó sẽ ưu tiên giá trị nào để thiết lập định kiểu cho thẻ.',
    'Synchronous (đồng bộ) là một quy trình xử lý các công việc theo một thứ tự đã được lập sẵn. Công việc sau được bắt đầu thực hiện chỉ khi công việc thứ nhất hoàn thành. Javascript là ngôn ngữ lập trình bất đồng bộ và chỉ chạy trên một luồng. Có những cách xử lý bất đồng bộ là: Callback, Promise, Asyn/Await.'
  ]

  return (
    <div className="App">
      <Radio.Group onChange={onChange} value={value} style={{display:'flex', justifyItems: 'flex-start', margin: '40px 0', marginLeft: '40px'}} size={'large'}>
        <Radio value={0}>Câu 1</Radio>
        <Radio value={1}>Câu 2</Radio>
        <Radio value={2}>Câu 3</Radio>
      </Radio.Group>
      <Tabs
        style={{marginLeft: '40px'}}

        defaultActiveKey="1"
        type="card"
        size={'large'}
        items={questions.map((content, i)=>{
          return {
            label: `Câu ${i+1}`,
            key: i,
            children: <div style={{width: '800px', margin: '0 auto',textAlign: 'left'}}>
                <h1 style={{borderBottom: '1px solid #ccc', marginBottom: 10}}>{questions[i]}</h1>
                <p style={{fontSize: 20}}>{answers[i]}</p>
            </div>
          }
        })}
      />
    </div>
  );
}

export default App;
